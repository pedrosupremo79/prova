﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace Classes_TCC
{
    public static class Banco
    {
        private static string Strconn;

        public static MySqlCommand Abrir()
        {
            Strconn =""; //ainda vamos fazer o banco, logo, não existe ainda string de conexão!
            MySqlConnection cn = new MySqlConnection(Strconn);
            cn.Open();
            MySqlCommand comm = new MySqlCommand();
            comm.Connection = cn;
            return comm;
        }
        public static void Fechar(MySqlCommand comm)
        {
            var cn = comm.Connection;
            if (cn.State == ConnectionState.Open)
            {
                comm.Connection.Close();
            }
        }
    }
}
